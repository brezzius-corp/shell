# Makefile

include config.mk

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic
EXECUTABLE=shell
EXECUTABLE_TEST=shell_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: shell
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/input.o $(SRC_BIN)/utils.o

test: package shell_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

shell: init main.o input.o utils.o
	$(CC) -o $(SRC_BIN)/shell $(SRC_BIN)/main.o $(SRC_BIN)/input.o $(SRC_BIN)/utils.o

shell_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/colors.h $(SRC_MAIN)/input.h $(SRC_MAIN)/utils.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

input.o: $(SRC_MAIN)/input.c $(SRC_MAIN)/input.h
	$(CC) -o $(SRC_BIN)/input.o -c $(SRC_MAIN)/input.c $(CFLAGS)

utils.o: $(SRC_MAIN)/utils.c $(SRC_MAIN)/utils.h
	$(CC) -o $(SRC_BIN)/utils.o -c $(SRC_MAIN)/utils.c $(CFLAGS)

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f $(SRC_BIN)/$(EXECUTABLE) ${DESTDIR}${PREFIX}/bin
	chown root:root ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)
	chmod 4755 ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)
	rm -f $(SRC_BIN)/$(EXECUTABLE_TEST)

