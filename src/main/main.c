#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <glob.h>
#include <sys/wait.h>

#include "colors.h"
#include "input.h"
#include "utils.h"

int main(void) {
    char *cmd, *arg_list[255], *p, *pp, *tmp, *times;
    char prompt[255], pathname[128] = "/home/tom";
    int i, j, retglob, cpt = 0;
    pid_t pid;
    glob_t gglob;

    while(1) {
        sprintf(prompt, "%sBrezzius%s %s[%s]%s %s[%s]%s %s$%s ", BOLD, RESET, CYAN, (times = hours()), RESET, GREEN, pathname, RESET, YELLOW, RESET);

        if(!(cmd = input(prompt))) {
            printf("\n");
            continue;
        }

        printf("\n");

        if(!strcmp("exit", cmd))
            return EXIT_SUCCESS;

        /* Traitement de la commande */
        for(i = 0 ; i < (int) strlen(cmd) ; i++)
            if(cmd[i] == ' ')
                cpt++;

        p = strdup(cmd);
        tmp = strtok(p, " ");

        i = 0;

        while(tmp) {
            if(!(pp = strstr(tmp, "*")))
                arg_list[i++] = strdup(tmp);
            else {
                if((retglob = glob(tmp, 0, NULL, &gglob)) == 0) {
                    for(j = 0 ; j < (int) gglob.gl_pathc ; j++)
                        arg_list[i++] = strdup(gglob.gl_pathv[j]);
                }

                globfree(&gglob);
            }

            tmp = strtok(NULL, " ");
        }

        arg_list[i] = NULL;

        while((pid = fork()) == -1) {
            switch(errno) {
                case EAGAIN:
                    break;
                case ENOMEM:
                    fprintf(stderr, "Create process : memory error\n");
                    exit(EXIT_FAILURE);
                    break;
                case ENOSYS:
                    fprintf(stderr, "Fork is not supported on this platform\n");
                    exit(EXIT_FAILURE);
                case ERESTART:
                    fprintf(stderr, "System call was interruped by a signal\n");
                    exit(EXIT_FAILURE);
                default:
                    break;
            }
        }

        switch(pid) {
            case 0:
                if((execvp(arg_list[0], arg_list)) == -1) {
                    switch(errno) {
                        case 2:
                            fprintf(stderr, "Command not found : %s\n", cmd);
                            break;
                        default:
                            fprintf(stderr, "Error execvp : [%d] -> %s\n", errno, strerror(errno));
                            break;
                    }

                    exit(EXIT_SUCCESS);
                }
                break;
            default:
                wait(&pid);
                break;
        }

        /* Free memory */ 
        i = 0;
        while(arg_list[i] != NULL)
            free(arg_list[i++]);

        free(p);
        free(cmd);
        free(times);
    }

    return EXIT_SUCCESS;
}

