#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <termios.h>

#include "input.h"
 
static int TIO_DESC = -1;
static struct termios TIO_ORIG;
static struct termios TIO_SETS;
static int CMD_MAX = 255;
 
/* Restore terminal to original settings */
void TIO_Free(void) {
    if(TIO_DESC != -1)
        tcsetattr(TIO_DESC, TCSANOW, &TIO_ORIG);
}
 
/* "Default" signal handler : restore terminal, then exit */
void TIO_Signal(int signum) {
    if(TIO_DESC != -1)
        tcsetattr(TIO_DESC, TCSANOW, &TIO_ORIG);
 
    /* exit() is not async-signal safe, but _exit() is.
     * Use the common idiom of 128 + signal number for signal exits.
     * Alternative approach is to reset the signal to default handler,
     * and immediately raise() it. */
    _exit(128 + signum);
}
 
/* Initialize terminal for non-canonical, non-echo mode,
 * that should be compatible with standard C I/O.
 * Returns 0 if success, nonzero errno otherwise.
*/
int TIO_Init(void) {
    struct sigaction act;
 
    /* Already initialized */
    if(TIO_DESC != -1)
        return errno = 0;
 
    /* Which standard stream is connected to TTY */
    if(isatty(STDERR_FILENO))
        TIO_DESC = STDERR_FILENO;
    else if(isatty(STDIN_FILENO))
        TIO_DESC = STDIN_FILENO;
    else if(isatty(STDOUT_FILENO))
        TIO_DESC = STDOUT_FILENO;
    else
        return errno = ENOTTY;
 
    /* Obtain terminal settings */
    if(tcgetattr(TIO_DESC, &TIO_ORIG) || tcgetattr(TIO_DESC, &TIO_SETS))
        return errno = ENOTSUP;
 
    /* Disable buffering for terminal streams. */
    if(isatty(STDIN_FILENO))
       setvbuf(stdin, NULL, _IONBF, 0);
    if(isatty(STDOUT_FILENO))
       setvbuf(stdout, NULL, _IONBF, 0);
    if(isatty(STDERR_FILENO))
       setvbuf(stderr, NULL, _IONBF, 0);
 
    /* At exit() or return from main(),
     * restore the original settings. */
    if(atexit(TIO_Free))
       return errno = ENOTSUP;
 
    /* Set new "default" handlers for typical signals,
     * so that if this process is killed by a signal,
     * the terminal settings will still be restored first. */
    sigemptyset(&act.sa_mask);
    act.sa_handler = TIO_Signal;
    act.sa_flags = 0;
    if(sigaction(SIGHUP,  &act, NULL) ||
       sigaction(SIGINT,  &act, NULL) ||
       sigaction(SIGQUIT, &act, NULL) ||
       sigaction(SIGTERM, &act, NULL) ||
#ifdef SIGXCPU
       sigaction(SIGXCPU, &act, NULL) ||
#endif
#ifdef SIGXFSZ    
       sigaction(SIGXFSZ, &act, NULL) ||
#endif
#ifdef SIGIO
       sigaction(SIGIO,   &act, NULL) ||
#endif
       sigaction(SIGPIPE, &act, NULL) ||
       sigaction(SIGALRM, &act, NULL))
       return errno = ENOTSUP;
 
    /* Let BREAK cause a SIGINT in input */
    TIO_SETS.c_iflag &= ~IGNBRK;
    TIO_SETS.c_iflag |=  BRKINT;
 
    /* Ignore framing and parity errors in input */
    TIO_SETS.c_iflag |=  IGNPAR;
    TIO_SETS.c_iflag &= ~PARMRK;
 
    /* Do not strip eighth bit on input */
    TIO_SETS.c_iflag &= ~ISTRIP;
 
    /* Do not do newline translation on input */
    TIO_SETS.c_iflag &= ~(INLCR | IGNCR | ICRNL);
 
#ifdef IUCLC
    /* Do not do uppercase-to-lowercase mapping on input */
    TIO_SETS.c_iflag &= ~IUCLC;
#endif
 
    /* Use 8-bit characters. This too may affect standard streams,
     * but any sane C library can deal with 8-bit characters */
    TIO_SETS.c_cflag &= ~CSIZE;
    TIO_SETS.c_cflag |=  CS8;
 
    /* Enable receiver. */
    TIO_SETS.c_cflag |=  CREAD;
 
    /* Let INTR/QUIT/SUSP/DSUSP generate the corresponding signals */
    TIO_SETS.c_lflag |=  ISIG;
 
    /* Enable noncanonical mode
     * This is the most important bit, as it disables line buffering etc. */
    TIO_SETS.c_lflag &= ~ICANON;
 
    /* Disable echoing input characters */
    TIO_SETS.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL);
 
    /* Disable implementation-defined input processing */
    TIO_SETS.c_lflag &= ~IEXTEN;
 
    /* To maintain best compatibility with normal behaviour of terminals,
     * we set TIME=0 and MAX=1 in noncanonical mode. This means that
     * read() will block until at least one byte is available */
    TIO_SETS.c_cc[VTIME] = 0;
    TIO_SETS.c_cc[VMIN] = 1;
 
    /* Set the new terminal settings.
     * Note that we don't actually check which ones were successfully
     * set and which not, because there isn't much we can do about it */
    tcsetattr(TIO_DESC, TCSANOW, &TIO_SETS);
 
    return errno = 0;
}

void TIO_CursorForward(int x) {
    if(x > 0)
        printf("\033[%dC", x);
}

void TIO_CursorBackward(int x) {
    if(x > 0)
        printf("\033[%dD", x);
}

void TIO_AddChar(char c, char *str, int *len, int *cursor) {
    int i;

    /* Re Allocated str for avoid overflow */
    if(*len > CMD_MAX) {
        CMD_MAX += 255;
        if(!(str = realloc(str, CMD_MAX * sizeof(char)))) {
            fprintf(stderr, "Memory error\n");
            exit(EXIT_FAILURE);
        }
    }

    if(*len == *cursor) {
        str[(*cursor)++] = c;
        str[++(*len)] = '\0';
    } else {
        for(i = *len-1 ; i >= *cursor ; i--)
            str[i+1] = str[i];

        str[(*cursor)++] = c;
        str[++(*len)] = '\0';
    }
}

void TIO_Refresh(char *str, char *prompt) {
    printf("\r\033[K%s%s", prompt, str);
    fflush(stdout);
}

char *input(char *prompt) {
    int c, t, i, running = 1;
    int cursor = 0, len = 0;
    char *str;

    if(TIO_Init()) {
        if(errno == ENOTTY)
            fprintf(stderr, "This program requires a terminal.\n");
        else
            fprintf(stderr, "Cannot initialize terminal: %s.\n", strerror(errno));

        return NULL;
    }

    /* Memory Allocation */
    if(!(str = malloc(CMD_MAX * sizeof(char)))) {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    printf("%s", prompt);
    while(running) {
        switch((c = getc(stdin))) {
            case 0x01: /* Ctrl A */
                if(cursor > 0)
                    TIO_CursorBackward(cursor);

                cursor = 0;
                break;
            case 0x02: /* Ctrl B */
                if(cursor > 0)
                    TIO_CursorBackward(1);

                cursor--;
                break;
            case 0x04: /* Ctrl D */
                if(len == cursor)
                    break;

                for(i = cursor ; i < len - 1 ; i++)
                    str[i] = str[i+1];

                str[--len] = '\0';

                TIO_Refresh(str, prompt);
                printf("\033[K");

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);
                break;
            case 0x05: /* Ctrl E */
                if(cursor < len) {
                    TIO_CursorForward(len - cursor);
                    cursor = len;
                }
                break;
            case 0x06: /* Ctrl F */
                if(cursor < len) {
                    TIO_CursorForward(1);
                    cursor++;
                }
                break;
            case 0x07: /* Ctrl G */
                printf("\a");
                break;
            case 0x08: /* Ctrl H */
                if(cursor <= 0)
                    break;

                for(i = cursor ; i < len ; i++)
                    str[i-1] = str[i];

                str[--len] = '\0';
                cursor--;

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);

                TIO_Refresh(str, prompt);
                printf("\033[K");

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);
                break;
            case 0x09: /* Ctrl I */
                break;
            case 0x0A: /* Ctrl J */
                running = 0;
                break;
            case 0x0B: /* Ctrl K */
                str[cursor] = '\0';
                printf("\033[K");
                len = cursor;
                break;
            case 0x0C: /* Ctrl L */
                printf("\033[H\033[2J");
                TIO_Refresh(str, prompt);
                break;
            case 0x0D: /* Ctrl M */
                running = 0;
                break;
            /*case 0x0E:
                printf("CTRL N\n");
                break;*/
            case 0X0F: /* Ctrl O */
                running = 0;
                break;
           /* case 0X10:
                printf("CTRL P\n");
                break;
            case 0x12:
                printf("CTRL R\n");
                break;*/
            case 0x14: /* Ctrl T */
                if(cursor == 0)
                    break;

                if(cursor == len) {
                    t = str[cursor - 1];
                    str[cursor - 1] = str[cursor - 2];
                    str[cursor - 2] = t;
                } else {
                    t = str[cursor];
                    str[cursor] = str[cursor - 1];
                    str[cursor - 1] = t;
                }

                TIO_Refresh(str, prompt);

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);
                break;
            case 0x15: /* Ctrl U */
                for(i = cursor ; i < len ; i++)
                    str[i - cursor] = str[i];

                str[i - cursor] = '\0';

                TIO_Refresh(str, prompt);

                len -= cursor;
                cursor = 0;

                if(len > 0)
                    TIO_CursorBackward(len);
                break;
            /*case 0x16:
                printf("CTRL V\n");
                break;*/
            case 0x17: /* Ctrl W */
                for(i = cursor ; i >= 0 ; i--) {
                    if(str[i] != ' ') {
                        if(i < len) {
                            str[i] = str[i+1];
                            str[i+1] = '\0';
                        } else
                            str[i] = '\0';

                        len--;
                        cursor--;
                    }
                    else
                        break;
                }
                TIO_Refresh(str, prompt);
                break;
           /* case 0x18:
                printf("CTRL X\n");
                break;
            case 0x19:
                printf("CTRL Y\n");
                break;
            case 0x03:
                printf("CTRL Z\n");
                break;*/
            case 0x7F: /* Backspace */
                if(cursor <= 0)
                    break;

                for(i = cursor ; i < len ; i++)
                    str[i-1] = str[i];

                str[--len] = '\0';
                cursor--;

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);

                TIO_Refresh(str, prompt);
                printf("\033[K");

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);
                break;
            case 0x1B: /* Arrow */
                if((getc(stdin)) == 91) {
                    switch(getc(stdin)) {
                        /*case 65:
                            printf("UP\n");
                            break;
                        case 66:
                            printf("DOWN\n");
                            break;*/
                        case 67:
                            if(cursor < len) {
                                TIO_CursorForward(1);
                                cursor++;
                            }
                            break;
                        case 68:
                            if(cursor > 0) {
                                TIO_CursorBackward(1);
                                cursor--;
                            }
                            break;
                    }
                }
                break;
            default:
                TIO_AddChar(c, str, &len, &cursor);

                TIO_Refresh(str, prompt);

                if(len - cursor > 0)
                    TIO_CursorBackward(len - cursor);
        }
    }

    if(!strcmp(str, ""))
        return NULL;
    else
        return str;
}

