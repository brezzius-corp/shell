#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utils.h"

void strreplace(char *s, char *s1, char *s2) {
    char *p, *copy, *tmp;
    int n = 0;

    p = s;
    tmp = strstr(p, s1);

    while(tmp) {
        n++;
        p = tmp + strlen(s1);
        tmp = strstr(p, s1);
    }

    if(n > 0) {
        if(!(copy = malloc(strlen(s) - (strlen(s1) * n) + (strlen(s2) * n) + 1))) {
            fprintf(stderr, "Memory error\n");
            exit(EXIT_FAILURE);
        }

        copy[0] = '\0';
        p = s;
        tmp = strstr(p, s1);

        while(tmp) {
            strncat(copy, p, tmp - p);
            strcat(copy, s2);
            p = tmp + strlen(s1);
            tmp = strstr(p, s1);
        }

        strcat(copy, p);
        strcpy(s, copy);

        free(copy);
    }
}

char *hours() {
    char *times;
    time_t timestamp;
    struct tm t;

    /* Allocation de times */
    if(!(times = malloc(6 * sizeof(char)))) {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    time(&timestamp);
    t = *localtime(&timestamp);

    strftime(times, 6, "%H:%M", &t);

    return times;
}

