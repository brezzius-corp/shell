#ifndef __INPUT_H
#define __INPUT_H

int TIO_Init(void);
void TIO_Free(void);
void TIO_Signal(int signum);
void TIO_CursorForward(int x);
void TIO_CursorBackward(int x);
void TIO_AddChar(char c, char *str, int *len, int *cursor);
void TIO_Refresh(char *str, char *prompt);
char *input(char *prompt);

#endif /* __INPUT_H */

