#ifndef _COLORS_H
#define _COLORS_H

#include <stdio.h>

/* Efface la console de l'utilisateur */
#define clrscr() printf("\033[H\033[2J")

/* Change la couleur du texte en fonction de la valeur de param.
   0 Réinitialisation          1 Texte en gras
   3 Texte en italique         4 Texte en souligné
   5 Clignotement              7 Vidéo inversé
   9 Texte barré
   30, 31, 32, 33, 34, 35, 36, 37 Couleurs des caractères
   40, 41, 42, 43, 44, 45, 46, 47 Couleurs du fond

   Les couleurs suivant la logique RGB, étant respectvement :
       noir, rouge, vert, jaune, bleu, magenta, cyan et blanc
*/
       
#define printc(param) printf("\033[%dm", param)

#define BOLD "\001\033[1;1m\002"
#define ITALIC "\001\033[1;3m\002"
#define BLACK "\001\033[1;30m\002"
#define RED "\001\033[1;31m\002"
#define GREEN "\001\033[1;32m\002"
#define YELLOW "\001\033[1;33m\002"
#define BLUE "\001\033[1;34m\002"
#define MAGENTA "\001\033[1;35m\002"
#define CYAN "\001\033[1;36m\002"
#define WHITE "\001\033[1;37m\002"
#define RESET "\001\033[0m\002"

#endif /* _COLORS_H */

