# Shell

Launch interractive shell

## Requirements

In order to build shell you need C89 library and header files.

## Installation

Edit config.mk to match your local setup (shell in installed into the /usr/local namespace by default).

Enter the following commands to download and build shell :

```
git clone https://gitlab.com/brezzius-corp/shell.git
cd shell
sudo make install
```

## Running

Enter the following command to run shell (if necessary as root) :

`shell [OPTIONS]`

## Options

No option is available

## Configuration

Edit config.h to match your shell configuration files and re(compiling) the source code.

## Uninstall

Enter the following command to uninstall shell :

`sudo make uninstall`
